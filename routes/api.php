<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Profile;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response;
use App\Models\bfr;
use App\Models\accessKeys;


Route::post('/createUser', function () {
    $profile = Profile::create([
        'last_date' => Carbon::now()->format('Y-m-d'),        
    ]);

    return response()->json($profile->id, 200);
});

Route::patch('/profiles/updateDate/', function (Request $request) {
    $id = $request->query('id');
    
    $user = Profile::find($id);
    if(!$user) return response(status: Response::HTTP_BAD_REQUEST);

    $difference = strtotime(Carbon::now()->format('Y-m-d')) - strtotime($user->last_date);
    $days = $difference / 86400;
    
    if (!($user->frequency)) return response()->json(false, Response::HTTP_OK);
    if ($days - $user->frequency >= 0) {    
        $user->update([
        'last_date' => Carbon::now()->format('Y-m-d')
        ]);  
        return response()->json(true, Response::HTTP_OK);    
    }
    return response()->json(false, Response::HTTP_OK);
});

Route::patch('/profiles/updateRemender/', function (Request $request) {
    $id = $request->query('id');
    
    $user = Profile::find($id);
    if(!$user) return response(status: Response::HTTP_BAD_REQUEST);

    $remender =  $request->query('remender');
    if($remender === null) return response(status: Response::HTTP_BAD_REQUEST);


    if($remender === 'false') {
        $user->update([
            'frequency' => null            
        ]);
        return response(status: Response::HTTP_NO_CONTENT);
    } else {
        $frequency =  $request->query('frequency');
        if(!$frequency) return response(status: Response::HTTP_BAD_REQUEST);
     
        $user->update([
        'frequency' => $frequency,
        'last_date' => Carbon::now()->format('Y-m-d')
    ]);
    return response(status: Response::HTTP_NO_CONTENT);
    };
    
});


Route::patch('/profiles/updatePro/', function (Request $request) {
    $id = $request->query('id');
    
    $user = Profile::find($id);
    if(!$user) return response(status: Response::HTTP_BAD_REQUEST);

    $code =  $request->query('code');
    $key = accessKeys::where('keys', $code)->first();
    if(!$key) return response()->json(false, Response::HTTP_OK); 
    if($key->profile_id) return response()->json(false, Response::HTTP_OK);

    
    $key->update([
        'profile_id' => $user->id,       
    ]);

    $user->update([
        'possibility' => true,        
    ]);

    return response()->json(true, Response::HTTP_OK);
    
});

Route::get('/profiles/getInfo/', function (Request $request) {

    $id = $request->query('id');
    
    $user = Profile::find($id);
  
   if(!$user) return response(status: Response::HTTP_BAD_REQUEST);

    if (($user->getAttribute('frequency'))) 
    { 
       $bool = true;    
       $frequency= $user->frequency;
    } else {
        $bool = false; 
        $frequency = 0;
    };

    return response()->json(["possibility" => $user->possibility, "remender" => $bool, "frequency" => $frequency], Response::HTTP_OK);
   
});

Route::get('/plants/', function (Request $request) {

    $stages = $request->query('stages');
    if ($stages > 0 && $stages < 4) {
        $id = 2;
    } else  if ($stages > 3 && $stages < 7) {
        $id = 3;
    } else if ($stages > 6 && $stages < 9) {
        $id = 4;
    } else {
        $id = 1;
    } 

    $plants = bfr::where('plant', $id)->get();;

    return response()->json($plants, Response::HTTP_OK);
});

Route::post('/autorizations', function (Request $request) {  
    $enter = $request->enter;
    if ($enter == env('ADMIN_PASSWORD')) {

        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        
        $length = strlen($chars);
        $string = '';
        for($i = 0; $i < 16; $i++) {
            $character = $chars[mt_rand(0, $length - 1)];
            $string .= $character;
        }               

        $key = accessKeys::create([
            'keys' => $string
            ]);  
        return "$string";
    } else {
        return redirect('/');;
    } 
});

