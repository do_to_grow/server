<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\bfr;

class BfrSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {      
        bfr::truncate();
       
               $csvFile = fopen(base_path('database/csvData/bfr.csv'), "r");
       
               $firstline = true;
               while (($data = fgetcsv($csvFile, 100, ";")) !== FALSE) {
                   if (!$firstline) {
                       bfr::create([
                           "plant" => $data['0'],
                           "stage" => $data['1'],
                           "src_frame" => $data['2'].'.png',
                       ]);
                   }
                   $firstline = false;
               }
       
               fclose($csvFile);
 
            }
}
    