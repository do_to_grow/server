<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;

class accessKeys extends Model
{
    use HasFactory;

    protected $table = 'accessKeys';

    protected $fillable = [
        'keys',
        'profile_id'        
    ];  

    protected $hidden = [
        'id'
    ]; 
    public $timestamps = false;
}
