<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;

class bfr extends Model
{
    use HasFactory;

    protected $table = 'bfr';

    protected $fillable = [
        'plant',
        'stage',
        'src_frame',
    ];  

    protected $hidden = [
        'id',
        'plant',
        'stage',        
    ]; 
    public $timestamps = false;
}
