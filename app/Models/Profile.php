<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $table = 'profile';

    protected $fillable = [
        'possibility',   
        'last_date',    
        'frequency',   
    ];  

    protected $hidden = [
        'id',                      
    ]; 
    
    protected $casts = [
        'possibility' => 'boolean',
    ];

    public $timestamps = false;
}
