<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>enter</title>
        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            .column {
                display: flex;
                flex-direction: column;
                flex-wrap: nowrap;
                height: 100px;
                align-items: center;
                        }

            .padding {
                margin: 8px;
                padding: 2px;
            }

            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body>
        <div>
            @csrf
            <form method="POST" class="column" action="/api/autorizations" >
                <input type="text" class="padding" name="enter">
                <button type="submit" >Вперед</button>
            </form>    
        </div>    
    </body>
</html>
